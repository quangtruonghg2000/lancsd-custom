# Lancs Agent Plugin Example

Name: lancs-agent-plugin-example
Version: 1.0.7
Release: 1%{dist}
License: GPLv3
Group: System/Plugins
Vendor: eGloo Incorporated
Packager: eGloo Incorporated
Source: %{name}-%{version}.tar.gz
BuildRoot: /var/tmp/%{name}-%{version}
Requires: lancsd >= 4.1.9
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
BuildRequires: lancsd-devel >= 4.1.9
BuildRequires: autoconf >= 2.63
BuildRequires: automake
BuildRequires: libtool
Summary: Example Plugin for the Lancs Agent DPI Engine

%description
This is an example plugin for the Lancs Agent DPI Engine.

Report bugs to: https://gitlab.com/lancs.ai/public/lancs-agent/issues

# Build
%prep
%setup -q
./autogen.sh
%{configure}

%build
make %{?_smp_mflags}

# Install
%install
make install DESTDIR=$RPM_BUILD_ROOT
rm -f ${RPM_BUILD_ROOT}/%{_libdir}/lib*.a
rm -f ${RPM_BUILD_ROOT}/%{_libdir}/lib*.la

# Clean-up
%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

# Post install
%post
/sbin/ldconfig

# Post uninstall
%postun
/sbin/ldconfig

# Files
%files
%defattr(-,root,root)
%{_libdir}/lib*.so*


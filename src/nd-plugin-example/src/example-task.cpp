// Lancs Agent Plugin Example
// Copyright (C) 2015-2022 eGloo Incorporated <http://www.egloo.ca>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdexcept>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <fstream>
#include <sstream>
#include <atomic>
#include <regex>
#include <mutex>

#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <dlfcn.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>

#define __FAVOR_BSD 1
#include <netinet/tcp.h>
#undef __FAVOR_BSD

#include <pcap/pcap.h>

#include <nlohmann/json.hpp>
using json = nlohmann::json;

using namespace std;

class ndPluginLoader;

#include <lancsd.h>
#include <nd-ndpi.h>
#include <nd-json.h>
#include <nd-util.h>
#include <nd-thread.h>
#include <nd-netlink.h>
#include <nd-apps.h>
#include <nd-protos.h>
#include <nd-risks.h>
#include <nd-category.h>
#include <nd-flow.h>
class ndFlowMap;
#include <nd-plugin.h>
#if (_ND_PLUGIN_VER > 0x20211111)
#include <nd-flow-map.h>
#endif
#include <nd-category.h>

#include "plugin-example.h"

ndPluginExampleTask::ndPluginExampleTask(const string &tag)
    : ndPluginTask(tag)
{
    nd_dprintf("%s: initialized\n", tag.c_str());
}

ndPluginExampleTask::~ndPluginExampleTask()
{
    Join();

    nd_dprintf("%s: destroyed\n", tag.c_str());
}

void *ndPluginExampleTask::Entry(void)
{
    ndJsonPluginParams params;

    nd_dprintf("%s: Hello, Plugin Task!\n", tag.c_str());

    PushReply(tag.c_str(), "Hello, Plugin Task!\n");

    while (PopParams(params)) {

        for (ndJsonPluginParams::const_iterator i = params.begin();
            i != params.end(); i++) {

            PushReplyLock(i->first, i->second);
        }
    }

    sleep(3);

    return NULL;
}

ndPluginInit(ndPluginExampleTask);

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4

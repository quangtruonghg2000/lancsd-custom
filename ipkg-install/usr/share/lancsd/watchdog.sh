#!/bin/sh

UPLOAD_WD="/var/run/lancsd/upload.wd"

[ ! -f "$UPLOAD_WD" ] && exit 0
 
if [ $[ $(date '+%s') - 30 ] -gt $(stat -c '%Y' "$UPLOAD_WD") ]; then
	/etc/init.d/lancsd restart
	#service lancsd restart
	#systemctl lancsd lancsd
fi

exit 0

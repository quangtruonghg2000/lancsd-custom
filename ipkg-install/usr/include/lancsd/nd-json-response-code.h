// Auto-generated JSON response codes 
// WARNING: All modifications will be LOST!

#ifndef _ND_JSON_RESPONSE_CODE_H
#define _ND_JSON_RESPONSE_CODE_H

enum ndJsonResponseCode
{
    ndJSON_RESP_NULL = 0,
    ndJSON_RESP_OK = 1,
    ndJSON_RESP_AUTH_FAIL = 2,
    ndJSON_RESP_MALFORMED_DATA = 3,
    ndJSON_RESP_SERVER_ERROR = 4,
    ndJSON_RESP_POST_ERROR = 5,
    ndJSON_RESP_PARSE_ERROR = 6,
    ndJSON_RESP_INVALID_RESPONSE = 7,
    ndJSON_RESP_INVALID_CONTENT_TYPE = 8,
    ndJSON_RESP_MAX = 9
};

#endif // _ND_JSON_RESPONSE_CODE_H

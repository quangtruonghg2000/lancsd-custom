# OpenWrt Official Packages Feed Build Notes

The easiest way to build the latest official release of the Lancs Agent is to do so from the official OpenWrt packages feed.

1. Clone the OpenWrt source:

  `# git clone https://github.com/openwrt/openwrt.git openwrt`

2. Enable the packages repository in: feeds.conf

3. Update packages feed:

  `# ./scripts/feeds update packages`

4. Install the Lancs Agent package source:

  `# ./scripts/feeds install lancsd`

5. Configure OpenWrt and enable the Lancs Agent under: __Network > lancsd__

  `# make menuconfig`
  
6. Build image and packages:

  `# make`

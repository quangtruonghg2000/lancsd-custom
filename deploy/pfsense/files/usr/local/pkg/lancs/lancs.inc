<?php
/*
 * lancs.inc
 *
 * Lancs for pfSense
 * Copyright (C) 2015-2022 eGloo Incorporated
 *
 * This program comes with ABSOLUTELY NO WARRANTY.
 *
 * This is free software, and you are welcome to redistribute it
 * under certain conditions according to the GNU General Public
 * License version 3, or (at your option) any later version.
 */

require_once("config.inc");
require_once("pfsense-utils.inc");
require_once("util.inc");
require_once('services.inc');
require_once("service-utils.inc");

require_once("/usr/local/pkg/lancs/lancs_defs.inc");

function lancs_create_rc() {
    $rcfile['file'] = 'lancsd.sh';
    $rcfile['start'] = 'mkdir -p ' .
        LANCSD_VOLATILE_STATEDIR . "\n\t" .
        LANCS_PREFIX . "etc/rc.d/lancsd onestart\n\t";
    $rcfile['stop'] = LANCS_PREFIX . "etc/rc.d/lancsd onestop\n\t";
    write_rcfile($rcfile);
}

function lancs_make_directories() {
    safe_mkdir(LANCSD_VOLATILE_STATEDIR);
}

function lancsd_is_running() {
    return isvalidpid(LANCSD_VOLATILE_STATEDIR . 'lancsd.pid');
}

function lancsd_get_uuid() {
    $uuid = '00-00-00-00';
    if (file_exists(LANCSD_UUID_AGENT))
        $uuid = trim(file_get_contents(LANCSD_UUID_AGENT));
    return $uuid;
}

function lancsd_get_agent_status_url() {
    $uuid = lancsd_get_uuid();
    return sprintf('%s%s', LANCS_URL_AGENT_STATUS, $uuid);
}

function lancsd_enable_sink($state = true) {
    $func = $state ? '--enable-sink' : '--disable-sink';
    $ph = popen("/usr/local/sbin/lancsd $func 2>/dev/null", 'r');
    if (! is_resource($ph)) return false;
    $buffer = stream_get_contents($ph);
    if (pclose($ph) != 0)
        return false;
    return true;
}

function lancsd_sink_enabled() {
    if (($conf = file_get_contents(LANCSD_CONFFILE)) !== false) {
        if (preg_match('/(enable_sink.*)/', $conf, $matches)) {
            if (($value = parse_ini_string(
                $matches[1], false, INI_SCANNER_TYPED)) !== false) {
                return $value['enable_sink'];
            }
        }
    }

    return false;
}

function lancs_package_sync() {
    lancs_make_directories();
    lancs_create_rc();
}

function lancs_package_install() {
    lancs_create_rc();
}

function lancs_package_deinstall() {
}
?>

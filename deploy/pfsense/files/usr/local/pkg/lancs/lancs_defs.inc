<?php
/*
 * lancs_defs.inc
 *
 * Lancs for pfSense
 * Copyright (C) 2015-2022 eGloo Incorporated
 *
 * This program comes with ABSOLUTELY NO WARRANTY.
 *
 * This is free software, and you are welcome to redistribute it
 * under certain conditions according to the GNU General Public
 * License version 3, or (at your option) any later version.
 */

if (! defined('LANCS_PREFIX'))
    define('LANCS_PREFIX', '/usr/local/');

if (! defined('LANCS_SBINDIR'))
    define('LANCS_SBINDIR', LANCS_PREFIX . 'sbin/');

if (! defined('LANCS_CONFDIR'))
    define('LANCS_CONFDIR', LANCS_PREFIX . 'etc/lancs.d/');

if (! defined('LANCSD_UUID_AGENT'))
    define('LANCSD_UUID_AGENT', LANCS_CONFDIR . 'agent.uuid');

if (! defined('LANCS_URL_MANAGER_API'))
    define('LANCS_URL_MANAGER_API', 'https://manager.lancs.ai/');

if (! defined('LANCS_URL_AGENT_STATUS')) {
    define('LANCS_URL_AGENT_STATUS',
        LANCS_URL_MANAGER_API . 'api/v1/deployment/agents/status/');
}

if (! defined('LANCSD_VOLATILE_STATEDIR'))
    define('LANCSD_VOLATILE_STATEDIR', '/var/run/lancsd/');

if (! defined('LANCSD_VERSION')) {
    $lancsver = exec_command(LANCS_SBINDIR .
        'lancsd --version 2>&1 | head -n 1 | cut -d\' \' -f2 | cut -d \'/\' -f2');
    define('LANCSD_VERSION', $lancsver);
}

if (! defined('LANCSD_CONFFILE'))
    define('LANCSD_CONFFILE', LANCS_PREFIX . 'etc/lancsd.conf');

if (! defined('LANCSD_JSON_STATUS'))
    define('LANCSD_JSON_STATUS', '/var/run/lancsd/status.json');
?>

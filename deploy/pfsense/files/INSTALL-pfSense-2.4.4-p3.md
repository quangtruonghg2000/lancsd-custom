# Lancs Agent
```
# pkg add http://pkg.freebsd.org/freebsd:11:x86:64/release_3/All/libunwind-20170615.txz
# pkg add http://pkg.freebsd.org/freebsd:11:x86:64/release_3/All/google-perftools-2.7.txz

# pkg add http://download.lancs.ai/lancs/freebsd/11.2/lancsd-2.97_1.txz
# pkg add http://download.lancs.ai/lancs/pfsense/2.4.4-p3/pfSense-pkg-lancs-1.0.1_1.txz
```

# Lancs FWA
```
# pkg add http://pkg.freebsd.org/freebsd:11:x86:64/release_3/All/python36-3.6.8_1.txz
# cd /usr/local/share
# git clone https://gitlab.com/lancs.ai/public/lancs-fwa.git
```

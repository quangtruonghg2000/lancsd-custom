#
# Copyright (C) 2016-2022 eGloo Incorporated
#
# This is free software, licensed under the GNU General Public License v2.

include $(TOPDIR)/rules.mk

PKG_NAME:=lancsd
PKG_RELEASE:=1
PKG_MAINTAINER:=Darryl Sokoloski <darryl@egloo.ca>
PKG_LICENSE:=GPL-3.0-or-later

PKG_BUILD_PARALLEL:=1
PKG_FIXUP:=autoreconf
PKG_INSTALL:=1

PKG_SOURCE_PROTO:=git
PKG_SOURCE_URL:=https://gitlab.com/lancs.ai/public/lancs-agent.git
PKG_SOURCE_DATE:=
PKG_SOURCE_VERSION:=v4.2.11
#PKG_SOURCE_VERSION:=
PKG_MIRROR_HASH:=whatever

include $(INCLUDE_DIR)/package.mk

define Package/lancsd
  SECTION:=net
  CATEGORY:=Network
  TITLE:=Lancs Agent
  URL:=http://www.lancs.ai/
  DEPENDS:=+ca-bundle +libcurl +libmnl +libnetfilter-conntrack +libpcap +zlib +libpthread @!USE_UCLIBC
  # Explicitly depend on libstdcpp rather than $(CXX_DEPENDS).  At the moment
  # std::unordered_map is only available via libstdcpp which is required for
  # performance reasons.
  DEPENDS+=+libstdcpp
endef

define Package/lancsd/description
The Lancs Agent is a deep-packet inspection server which detects network
protocols and applications.  These detections can be saved locally, served over
a UNIX or TCP socket, and/or "pushed" (via HTTP POSTs) to a remote third-party
server.  Flow metadata, network statistics, and detection classifications are
JSON encoded for easy consumption by third-party applications.
endef

define Package/lancsd/conffiles
/etc/lancsd.conf
/etc/config/lancsd
/etc/lancs.d/agent.uuid
/etc/lancs.d/serial.uuid
/etc/lancs.d/site.uuid
endef

TARGET_CFLAGS+=-ffunction-sections -fdata-sections -Wno-psabi
TARGET_CXXFLAGS+=-ffunction-sections -fdata-sections -Wno-psabi
TARGET_LDFLAGS+=-Wl,--gc-sections

CONFIGURE_ARGS+= \
	--sharedstatedir=/var/run \
	--enable-lean-and-mean \
	--disable-libtcmalloc \
	--disable-jemalloc \
	--without-systemdsystemunitdir \
	--without-tmpfilesdir

# Disable configuration file-watch support (deprecated feature).
# Not to be confused with kernel/system-level inotify support.
CONFIGURE_ARGS+= \
	--disable-inotify

ifneq ($(CONFIG_LIBCURL_ZLIB),y)
CONFIGURE_ARGS+= \
	--without-libcurl-zlib
endif

define Build/Configure
	(cd $(PKG_BUILD_DIR); ./autogen.sh)
	$(call Build/Configure/Default)
endef

define Build/InstallDev
	$(INSTALL_DIR) $(1)/usr/include/lancsd
	$(CP) $(PKG_INSTALL_DIR)/usr/include/lancsd/*.h $(1)/usr/include/lancsd
	$(INSTALL_DIR) $(1)/usr/include/lancsd/pcap-compat
	$(CP) $(PKG_INSTALL_DIR)/usr/include/lancsd/pcap-compat/*.h $(1)/usr/include/lancsd/pcap-compat
	$(INSTALL_DIR) $(1)/usr/include/lancsd/nlohmann
	$(CP) $(PKG_INSTALL_DIR)/usr/include/lancsd/nlohmann/*.hpp $(1)/usr/include/lancsd/nlohmann
	$(INSTALL_DIR) $(1)/usr/include/ndpi
	$(CP) $(PKG_INSTALL_DIR)/usr/include/ndpi/*.h $(1)/usr/include/ndpi
	$(INSTALL_DIR) $(1)/usr/lib
	$(CP) $(PKG_INSTALL_DIR)/usr/lib/liblancsd.{a,so*} $(1)/usr/lib
	$(INSTALL_DIR) $(1)/usr/lib/pkgconfig
	$(INSTALL_DATA) $(PKG_INSTALL_DIR)/usr/lib/pkgconfig/liblancsd.pc $(1)/usr/lib/pkgconfig
endef

define Package/lancsd/install
	$(INSTALL_DIR) $(1)/etc
	$(INSTALL_DATA) $(PKG_BUILD_DIR)/deploy/lancsd.conf $(1)/etc
	$(INSTALL_DIR) $(1)/etc/config
	$(INSTALL_CONF) ./files/lancsd.config $(1)/etc/config/lancsd
	$(INSTALL_DIR) $(1)/etc/init.d
	$(INSTALL_BIN) ./files/lancsd.init $(1)/etc/init.d/lancsd
	$(INSTALL_DIR) $(1)/usr/sbin
	$(INSTALL_BIN) $(PKG_INSTALL_DIR)/usr/sbin/lancsd $(1)/usr/sbin
	$(INSTALL_DIR) $(1)/usr/lib
	$(CP) $(PKG_INSTALL_DIR)/usr/lib/liblancsd.so.* $(1)/usr/lib
	$(INSTALL_DIR) $(1)/etc/lancs.d
	$(INSTALL_DATA) $(PKG_BUILD_DIR)/deploy/lancs-apps.conf $(1)/etc/lancs.d
	$(INSTALL_DATA) $(PKG_BUILD_DIR)/deploy/lancs-categories.json $(1)/etc/lancs.d
	$(INSTALL_DIR) $(1)/usr/share/lancsd
	$(INSTALL_DATA) $(PKG_BUILD_DIR)/deploy/functions.sh $(1)/usr/share/lancsd
endef

$(eval $(call BuildPackage,lancsd))
